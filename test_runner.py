import logging
import os
import sys
import unittest


def main(sdk_path, test_path):
    sys.path.insert(0, sdk_path)
    import dev_appserver
    dev_appserver.fix_sys_path()
    suite = unittest.loader.TestLoader().discover(test_path)
    unittest.TextTestRunner(verbosity=2).run(suite)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    SDK_PATH = os.getenv('SDK_PATH', os.path.expanduser('~/google_appengine/'))
    TEST_PATH = os.getenv('TEST_PATH', os.getcwd())
    logging.info('SDK_PATH: %s', SDK_PATH)
    logging.info('TEST_PATH: %s', TEST_PATH)
    main(SDK_PATH, TEST_PATH)
