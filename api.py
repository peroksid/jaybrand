from __future__ import division

import webapp2
from webapp2_extras import json
from google.appengine.ext import db
from google.appengine.api import memcache


def division_op(dividend, divisor):
    return json.encode({'quotient': dividend / divisor,
                        'result': dividend // divisor,
                        'modulus': dividend % divisor})


class DivisionOp(db.Model):
    op_key = db.StringProperty()
    data = db.StringProperty()
    

class ApiHandler(webapp2.RequestHandler):
    def get(self, divisor):
        # Create the handler's response "Hello World!" in plain text.
        self.response.headers['Content-Type'] = 'application/json'
        errors = []
        if 'dividend' not in self.request.GET:
            errors.append('Missing dividend.')
        else:
            try:
                df = float(self.request.GET['dividend'])
                di = int(self.request.GET['dividend'])
            except ValueError:
                errors.append(
                    'Error converting divident "%s" to number.' %
                    self.request.GET['dividend'])
            else:
                if df == di:
                    dividend = di
                else:
                    dividend = df
        
        divisor = int(divisor)
        if divisor == 0:
            errors.append('Division by zero.')
            
        if errors:
            result = json.encode({'errors': errors})
        else:
            op_key = '{}:{}'.format(dividend, divisor)
            data = memcache.get(op_key)
            if data is None:
                qs = DivisionOp.all().filter('op_key ==', op_key)
                if qs.count():
                    data = qs.fetch(0).data
                else:
                    data = division_op(dividend, divisor)
                    DivisionOp(op_key=op_key, data=data).put()
                memcache.add(op_key, data, 60)
            result = data
        self.response.out.write(result)
