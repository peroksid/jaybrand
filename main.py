import webapp2


from api import ApiHandler


application = webapp2.WSGIApplication([
    ('/divide/(\d+)/', ApiHandler)
], debug=True)
