import unittest

import webapp2
import webtest
from webapp2_extras import json

from api import ApiHandler

from google.appengine.ext import testbed


class ApiTest(unittest.TestCase):
    def setUp(self):
        app = webapp2.WSGIApplication([('/divide/(\d+)/', ApiHandler)])
        self.testapp = webtest.TestApp(app)
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()

    def testOkExample(self):
        response = self.testapp.get('/divide/4/?dividend=10')
        self.assertEqual(response.status_int, 200)
        self.assertEqual(response.normal_body, json.encode({
            'quotient': 2.5,
            'result': 2,
            'modulus': 2
        }))
        self.assertEqual(response.content_type, 'application/json')

    def testModulus(self):
        response = self.testapp.get('/divide/3/?dividend=10')
        self.assertEqual(response.status_int, 200)
        self.assertEqual(response.normal_body, json.encode({
            'quotient': 10 / 3.,
            'result': 3,
            'modulus': 1
        }))
        self.assertEqual(response.content_type, 'application/json')

    def testZeroDivisor(self):
        response = self.testapp.get('/divide/0/?dividend=42')
        self.assertEqual(response.status_int, 200)
        self.assertEqual(response.normal_body, json.encode({
            "errors": ["Division by zero."]
        }))
        self.assertEqual(response.content_type, 'application/json')

    def testMissingDividend(self):
        response = self.testapp.get('/divide/42/')
        self.assertEqual(response.status_int, 200)
        self.assertEqual(response.normal_body, json.encode({
            "errors": ["Missing dividend."]
        }))
        self.assertEqual(response.content_type, 'application/json')

    def testBadDividend(self):
        response = self.testapp.get('/divide/42/?dividend=')
        self.assertEqual(response.status_int, 200)
        self.assertEqual(response.normal_body, json.encode({
            "errors": ['Error converting divident "" to number.']
        }))
        self.assertEqual(response.content_type, 'application/json')

        response = self.testapp.get('/divide/42/?dividend=aa')
        self.assertEqual(response.status_int, 200)
        self.assertEqual(response.normal_body, json.encode({
            "errors": ['Error converting divident "aa" to number.']
        }))
        self.assertEqual(response.content_type, 'application/json')
